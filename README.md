# globalauth

> Driver for Global Auth API

API is live at https://globalauth.herokuapp.com/

## Usage
### Import
```
const { GlobalAuthDriver } = require('globalauth');
```
### Register
```
GlobalAuthDriver.register({
  apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj',
  config: { verifySessionURL: '/glauthVerifySession' },
  app
});
```
### Listen to API Requests
```
GlobalAuthDriver.instance.setup();
```
### Or invoke functions youself
```
GlobalAuthDriver.instance.verifySession('############');
```

## Get Trial API Key
> To get an API Key, send email with you company/organization details at `freemaileronline@gmail.com`.  
Details should contain, Company full name, public email address and private email address.