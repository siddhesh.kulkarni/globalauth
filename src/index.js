const axios = require('axios');

class InvalidSessionError extends Error {
  constructor(message) {
    super(message);
    this.name = "InvalidSessionError";
    this.httpCode = 401;
  }
}

/**
 * @typedef {{
 * id: number;
 * fullName: string;
 * username: string;
 * dob: Date;
 * email: string;
 * googleId: string;
 * clientId: number;
 * role: string;
 * permissions: string[];
 * }} CUser
 */

/**
 * @typedef {{
 *   message: string;
 *   data: any;
 *   code: number;
 *   errorDetails: Error;
 *   new(): ICError;
 * }} ICError
 */

/**
 * @typedef {{
 * logText: string;
 * clientName: string;
 * logTime: string;
 * }} LogsTemplateData
 */

/**
 * @typedef {{
 * log:(...args: any[]) => Promise<any>;
 * info:(...args: any[]) => Promise<any>;
 * warn:(...args: any[]) => Promise<any>;
 * error:(...args: any[]) => Promise<any>
 * }} ILogger
 */

/**
 * @typedef {{
 * (cb: (request: any, response: any, next: any) => void) => (request: any, response: any, next: any) => Promise<void>;
 * }}  RespWrapper
 */

/**
 * @typedef {{
 * apiKey: string;
 * url?: string;
 * app: Express;
 * wrapper?: RespWrapper;
 * config: { verifySessionURL: string; endSessionURL: string; };
 * CError?: typeof ICError;
 * ErrorCodes?: {[id:string]: number;};
 * logger?: ILogger;
 * }} GlobalAuthDriverAttributes
 */

/**
 * @class GlobalAuthDriver
 * Driver class for Global Auth API @ https://globalauth.herokuapp.com/
 */
class GlobalAuthDriver {
  /**
   * Singleton static email service instance
   * @type {GlobalAuthDriver}
   * @static
   */
  static instance = null;

  /**
   * Creates a singleton class  
   * Use `GlobalAuthDriver.register({})` instead of `new GlobalAuthDriver()`
   * @private
   * @param {GlobalAuthDriverAttributes} attr 
   */
  constructor(attr = {}) {
    if (!GlobalAuthDriver.instance) {
      GlobalAuthDriver.instance = this;
    } else {
      throw new Error('GlobalAuthDriver instance already available, please use `GlobalAuthDriver.instance`');
    }
    this.apiKey = "";
    this.url = "https://globalauth.herokuapp.com";

    this.config = {
      verifySessionURL: '',
      endSessionURL: ''
    };

    /**
     * @type {ICError}
     */
    this.CError;

    /**
     * @type {{[id:string]: number;}}
     */
    this.ErrorCodes = { 1: 'SUCCESS', SUCCESS: 1, 0: 'INTERNAL_ERROR', INTERNAL_ERROR: 0 };

    /**
     * @type {RespWrapper}
     */
    this.wrapper = async (cb) => async (req, resp, next) => await cb(req, resp, next);

    /**
     * @type {ILogger}
     */
    this.logger = {
      error: () => { },
      info: () => { },
      log: () => { },
      warn: () => { }
    };

    Object.assign(this, attr);
  }

  /**
   * Setsup the environment with API Endpoints.
   * setup is simple, once it is properly registered
   * @throws InvalidSessionError
   * @example GlobalAuthDriver.instance.setup();
   */
  setup() {
    this.app.post(this.config.verifySessionURL, this.wrapper(async (request, response, next) => {
      const out = await this.verifySession(request.body.authToken);
      if (!out) {
        throw new InvalidSessionError('Session is invalid');
      }

      response.status(200).send(new this.CError({ message: 'Success', data: out, code: this.ErrorCodes.SUCCESS }));
    }));

    this.app.post(this.config.endSessionURL, this.wrapper(async (request, response, next) => {
      const out = await this.endSession(request.body.authToken);

      response.status(200).send(new this.CError({ message: 'Success', data: {}, code: this.ErrorCodes.SUCCESS }));
    }));
  }

  /**
   * Creates a singleton class.
   * @example
   * GlobalAuthDriver.register({
   *   apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj',
   *   config: { verifySessionURL: '/glauthVerifySession' },
   *   app
   * });
   * @param {GlobalAuthDriverAttributes} attr 
   * @returns {GlobalAuthDriver} Singleton instance of GlobalAuthDriver
   */
  static register(attr) {
    return new GlobalAuthDriver(attr);
  }

  /**
   * Verifies auth token against global auth api
   * @param {string} authToken 24 length auth token
   */
  async verifySession(authToken) {
    const data = {
      apiKey: this.apiKey,
      authToken
    };

    const out = await axios.post(this.url + "/verifysession", data);
    /**
     * @type {ICError}
     */
    const resp = out.data;

    if (resp.code == this.ErrorCodes.SUCCESS) {
      return resp.data;
    } else {
      return void 0;
    }
  }

  /**
   * Ends session with authToken
   * @param {string} authToken 24 length auth token
   * @returns Promise<any>
   */
  async endSession(authToken) {
    const data = {
      apiKey: this.apiKey,
      authToken
    };

    return await axios.post(this.url + "/endsession", data);
  }

  /**
   * Returns all users data registered under current client
   * @returns 
   */
  async getAllUseres() {
    const data = { apiKey: this.apiKey };

    const out = await axios.post(this.url + "/getallusers", data);
    /**
     * @type {ICError}
     */
    const resp = out.data;

    if (resp.code == this.ErrorCodes.SUCCESS) {
      /**
       * @type {CUser}
       */
      const users = resp.data.users;
      return users;
    } else {
      this.logger.warn(resp.message);
      return void 0;
    }
  }
}

module.exports = { GlobalAuthDriver, InvalidSessionError };
